define apache2::vhost( "mysite.example.com":
	 $port 	       = '80',
        $docroot       = '/var/www/example',
       #$template      = '/home/zabbix/puppet/modules/apache2/files/example
        $priority      = '25',
        $servername    = 'mysite.example.com',
        $serveraliases = '',
        $options       = "Indexes FollowSymLinks MultiViews",
        $vhost_name    = 'example',
	$vdit	       = '/etc/apache2/sites-available/',
) 
{
       include apache2

	file {'/etc/apache2/sites-available':
		ensure=> 'link',
		target=> '/etc/apache2/sites-enabled/'
        }       

      	file {"/var/www/example/index.html":
        	require => Package['apache2'],
        	notify  => Service['apache2'],
		ensure => present,
		source => "puppet:///modules/apache2/files/example/index.html"
        }	
	service {"apache2":
		require => Package["apache2"],
		ensure => running
	}
}
