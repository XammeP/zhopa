class apache2 {
	package {"apache2":
	 ensure => installed,
	}

	file {"/etc/apache2/sites-enabled/000-default":
		require => Package["apache2"],
		notify => Service["apache2"],
		ensure => present,
		source => "puppet:///modules/apache2/000-default"
	}

	service {"apache2":
		require => Package["apache2"],
		ensure => running,
	}

}


