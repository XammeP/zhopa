include apt
apt::source { 'ftp_ru_debian_org':
  location   => 'http://ftp.ru.debian.org/debian/',
  release    => 'squeeze',
  repos      => 'main',
}
